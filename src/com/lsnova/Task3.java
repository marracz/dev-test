package com.lsnova;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

public class Task3 {

    public static void main(String[] args) {

        // Task 3
        Scanner scanner = new Scanner(System.in).useDelimiter("\\s");

        List<Set<Integer>> graphs = new ArrayList<>();
        int lines = scanner.nextInt();
        for (int i = 0; i < lines; i++) {
            int vertice1 = scanner.nextInt();
            int vertice2 = scanner.nextInt();

            Optional<Set<Integer>> graphForVertice1 = findGraphForVertice(vertice1, graphs);
            Optional<Set<Integer>> graphForVertice2 = findGraphForVertice(vertice2, graphs);
            if (graphForVertice1.isPresent() && graphForVertice2.isPresent()) {
                connectGraphs(graphs, graphForVertice1.get(), graphForVertice2.get());
            } else if (graphForVertice1.isPresent()) {
                assignToGraph(graphForVertice1.get(), vertice2);
            } else if (graphForVertice2.isPresent()) {
                assignToGraph(graphForVertice2.get(), vertice1);
            } else {
                createGraph(graphs, vertice1, vertice2);
            }
        }
        System.out.println(graphs.size());

    }

    private static void createGraph(List<Set<Integer>> graphs, int vertice1, int vertice2) {
        Set<Integer> graph = new HashSet<>();
        graph.add(vertice1);
        graph.add(vertice2);
        graphs.add(graph);
    }

    private static void assignToGraph(Set<Integer> graph, int vertice) {
        graph.add(vertice);
    }

    private static void connectGraphs(List<Set<Integer>> graphs,
                                      Set<Integer> graphForVertice1,
                                      Set<Integer> graphForVertice2) {
        Set<Integer> graph = new HashSet<>(graphForVertice1);
        graph.addAll(graphForVertice2);
        graphs.remove(graphForVertice1);
        graphs.remove(graphForVertice2);
        graphs.add(graph);
    }

    private static Optional<Set<Integer>> findGraphForVertice(int vertice1, List<Set<Integer>> graphs) {
        return graphs.stream().filter(graph -> graph.contains(vertice1)).findAny();
    }
}
