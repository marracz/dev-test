package com.lsnova;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Task1 {

    public static void main(String[] args) {

        // Task 1
        Scanner scanner = new Scanner(System.in).useDelimiter("\\s");
        int size = 0;
        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;
        Set<Integer> distinct = new TreeSet<>();
        while (scanner.hasNextInt()) {
            int number = scanner.nextInt();
            size++;
            distinct.add(number);
            if (max < number) {
                max = number;
            }
            if (min > number) {
                min = number;
            }
        }
        System.out.println(distinct.stream().map(Object::toString).collect(Collectors.joining(" ")));
        System.out.println("count: " + size);
        System.out.println("distinct: " + distinct.size());
        System.out.println("min: " + min);
        System.out.println("max: " + max);

    }
}
