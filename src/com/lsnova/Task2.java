package com.lsnova;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {

        // Task 2
        Scanner scanner = new Scanner(System.in).useDelimiter("\\s");

        Map<Integer, Integer> numbers = new HashMap<>();
        List<Pair> pairs = new ArrayList<>();
        while (scanner.hasNextInt()) {
            int number = scanner.nextInt();
            int factor = 13 - number;

            if (numbers.containsKey(factor)) {
                Pair pair = new Pair(number, factor);
                Integer occurrence = numbers.get(factor);
                for (int i=0; i<occurrence; i++) {
                    pairs.add(pair);
                }
            }
            numbers.computeIfPresent(number, (k,v) -> v + 1);
            numbers.putIfAbsent(number, 1);
        }
        pairs.sort(Comparator.comparing(x -> x.smaller));
        pairs.forEach(System.out::println);
    }

    private static class Pair {
        private final int smaller;
        private final int larger;

        public Pair(int x, int y) {
            if (x<y) {
                this.smaller = x;
                this.larger = y;
            }
            else {
                this.smaller = y;
                this.larger = x;
            }
        }

        @Override
        public String toString() {
            return this.smaller + " " + this.larger;
        }
    }
}
